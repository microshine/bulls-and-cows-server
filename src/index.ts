import * as http from "http";
const SERVER_PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
const SERVER_IP_ADDRESS = process.env.OPENSHIFT_NODEJS_IP || "0.0.0.0";

const app = http.createServer((req, res) => {
    res.end("Success");
})
.on("listening", () => {
    console.log(`Server started on ${SERVER_IP_ADDRESS}:${SERVER_PORT} at new ${Date()}`);
})
.on("close", () => {
    console.log("Server closed at", new Date());
})
.on("error", (error) => {
    console.log(error);
})

app.listen(SERVER_PORT, SERVER_IP_ADDRESS);